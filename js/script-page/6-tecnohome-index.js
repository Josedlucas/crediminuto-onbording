$('#sendRequest').on('click', function (event) {
    event.preventDefault();
    $(this).addClass('disabled')
    countBack(); //Reiniciomos el cod
});

function countBack() {
    var fiveSeconds = new Date().getTime() + 59000;
    $('#time-code').countdown(fiveSeconds, {elapse: true})
    .on('update.countdown', function(event) {
        var $this = $(this);
        if (!event.elapsed) {
            $this.html(event.strftime('<span>El código enviado vence en: %M:%S</span>'));
        }

        if (event.offset.seconds == 0){
            // EL TIEMPO TERMINO LLAMAR A FUNCIONA PARA VALIDAR QUE TERMINO EL TIEMPO CON EL BACKEND
            $('#time-code').countdown('stop');
            $('#sendRequest').removeClass('disabled'); // Habilitamos el boton de reenviar codigo
            $('#sendRequest').html('Reenviar código');
            $('#time-code').html('');
        }
    });
}